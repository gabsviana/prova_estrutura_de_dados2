package br.ucsal;

import java.util.Scanner;

import br.ucsal.models.Teacher;
import br.ucsal.queue.Queue;
import br.ucsal.stack.Stack;

public class ATV1 {
	private static Scanner sc = new Scanner(System.in);

	private static Stack<Teacher> stack = new Stack<Teacher>();
	private static Queue<Teacher> queue = new Queue<Teacher>();

	public static void addStack(Teacher teacher) {
		stack.push(teacher);
	}

	public static void addQueue(Teacher teacher) {
		queue.add(teacher);
	}

	private static void inicio() {
		char option = '.';
		while (option != 's' || option != 'S') {
			System.out.println("Selecione a op��o desejada: ");
			System.out.println("(a) Adicionar professor\n(f) Listar Fila\n(p) Listar pilha\n(s) Sair do Programa");
			option = sc.next().charAt(0);
			System.out.println();
			if (option == 'a' || option == 'A') {
				System.out.println("Digite o nome do Professor:");
				String name = sc.next();
				System.out.println("Digite a Area de Ensino do Professor: ");
				char teachingArea = sc.next().charAt(0);
				if (teachingArea == 'H') {
					queue.add(new Teacher(name, teachingArea));
				} else {
					stack.push(new Teacher(name, teachingArea));
				}
			} else if (option == 'f' || option == 'F') {
				queue.print();
			} else if (option == 'p' || option == 'P') {
				stack.print();
			} else if (option == 'S' || option == 's') {
				System.out.println("Fim do programa");
				return;
			} else {
				System.out.println("Valor inserido � inv�lido!\nTente novamente\n\n");
			}
		}
	}

	public static void main(String[] args) {
		inicio();
	}
}
