package br.ucsal.stack;

public class Stack<T> {
	
	private No<T> top;
	private Integer length = 0;
	
	public void push(T teacher) {
		No<T> newNode = new No<T>(teacher, this.top);
		this.top = newNode;
		this.length += 1;
	}

	public void print() {
		No<T> aux = this.top;
		while (aux != null) {
			System.out.println(aux.getObject());
			aux = aux.getAnt();
		}
	}
}