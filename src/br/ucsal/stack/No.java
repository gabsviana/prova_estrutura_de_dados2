package br.ucsal.stack;

public class No<T> {
	private T object;
	private No<T> ant;

	public No(T object, No<T> ant) {
		super();
		this.object = object;
		this.ant = ant;
	}

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	public No<T> getAnt() {
		return ant;
	}

	public void setAnt(No<T> ant) {
		this.ant = ant;
	}
}
