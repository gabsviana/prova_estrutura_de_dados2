package br.ucsal.models;

public class Teacher {
	private String name;
	private char teachingArea;

	public Teacher(String name, char teachingArea) {
		this.name = name;
		this.teachingArea = teachingArea;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getTeachingArea() {
		return teachingArea;
	}

	public void setTeachingArea(char teachingArea) {
		this.teachingArea = teachingArea;
	}

	@Override
	public String toString() {
		return "Teacher [name=" + name + ", teachingArea=" + teachingArea + "]";
	}

}
