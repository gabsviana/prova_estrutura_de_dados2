package br.ucsal.queue;

public class Queue<T> {
	private No<T> head;
	private No<T> tail;
	private int size;

	private T get(int index) {
		if (!(index >= 0 && index < this.size))
			throw new IllegalArgumentException("Erro");
		No<T> aux = this.head;
		for (int i = 0; i < index; i++)
			aux = aux.getProx();
		return aux.getObject();
	}

	public void add(T object) {
		if (this.size == 0) {
			No<T> newNode = new No<T>(this.head, object);
			this.head = newNode;
			if (size == 0) {
				this.tail = this.head;
			}
		} else {
			No<T> newNode = new No<T>(object);
			this.tail.setProx(newNode);
			this.tail = newNode;
		}
		this.size += 1;
	}

	public void print() {
		for (int i = 0; i < size; i++)
			System.out.println(get(i));
	}
}
