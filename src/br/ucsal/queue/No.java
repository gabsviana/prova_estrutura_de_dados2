package br.ucsal.queue;

public class No<T> {
	private No<T> prox;
	private T object;

	public No(No<T> prox, T object) {

		this.prox = prox;
		this.object = object;
	}

	public No(T object) {
		this.object = object;
	}

	public No<T> getProx() {
		return prox;
	}

	public void setProx(No<T> prox) {
		this.prox = prox;
	}

	public T getObject() {
		return object;
	}
}
